import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.2.5.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"

    kotlin("jvm") version "1.3.61"
    kotlin("plugin.spring") version "1.3.61"
}

group = "io.upservice"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

val junitVersion = "5.6.0"
val swaggerVersion = "1.6.0"
val jwtVersion = "0.11.0"
val springFoxVersion = "2.9.2"

repositories {
    jcenter()
    mavenCentral()
}

dependencies {
    /** Kotlin */
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    /** Spring */
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-security")

    /** Database */
    implementation("com.h2database:h2")
    implementation("org.postgresql:postgresql")

    /** JWT */
    implementation("io.jsonwebtoken:jjwt-impl:$jwtVersion")
    implementation("io.jsonwebtoken:jjwt-api:$jwtVersion")

    /** Swagger */
    implementation("io.springfox:springfox-swagger2:$springFoxVersion")
    implementation("io.springfox:springfox-swagger-ui:$springFoxVersion")
    implementation("io.swagger:swagger-annotations:$swaggerVersion")
    implementation("io.swagger:swagger-models:$swaggerVersion")

    testImplementation("org.junit.jupiter:junit-jupiter:$junitVersion")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
