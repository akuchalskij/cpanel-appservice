package io.upservice.cpanel.commands

import java.io.File
import java.util.concurrent.TimeUnit

open class BaseCommand {
    fun createFile(pathName: String?, fileName: String?, extension: String?, content: String): File {
        val file = File("$pathName/$fileName.$extension")

        file.writeText(content)

        return file
    }

    protected fun String.runCommand(workingDir: File) {
        ProcessBuilder(*split(" ").toTypedArray())
            .directory(workingDir)
            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()
            .waitFor(60, TimeUnit.MINUTES)
    }
}