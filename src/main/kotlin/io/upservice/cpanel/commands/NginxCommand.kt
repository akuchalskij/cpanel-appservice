package io.upservice.cpanel.commands

import io.upservice.cpanel.domain.users.WebSite
import java.io.File

class NginxCommand : BaseCommand() {
    fun createDomainConfig(webSite: WebSite, template: String): Unit {
        val config = createFile("/etc/nginx/sites-available", webSite.serverName, "conf", template)

        "ln -s /etc/nginx/sites-available/${webSite.serverName}.conf /etc/nginx/sites-enabled".runCommand(config)
        "nginx -t".runCommand(config)

        reload(config)
    }

    fun generateSSL(serverName: String? = "", dir: File): Unit {
        return "certbot -d $serverName -d www.$serverName".runCommand(dir)
    }

    private fun reload(dir: File): Unit {
        return "service nginx reload".runCommand(dir)
    }
}