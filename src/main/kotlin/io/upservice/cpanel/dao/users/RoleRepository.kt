package io.upservice.cpanel.dao.users

import io.upservice.cpanel.domain.users.Role
import org.springframework.data.jpa.repository.JpaRepository

interface RoleRepository : JpaRepository<Role, Long> {
    fun findByName(name: String): Role
}