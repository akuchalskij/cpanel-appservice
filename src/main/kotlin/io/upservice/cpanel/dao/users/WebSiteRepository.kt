package io.upservice.cpanel.dao.users

import io.upservice.cpanel.domain.users.WebSite
import org.springframework.data.jpa.repository.JpaRepository

interface WebSiteRepository : JpaRepository<WebSite, Long> {
}