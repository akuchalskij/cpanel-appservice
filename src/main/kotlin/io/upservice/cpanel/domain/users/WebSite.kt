package io.upservice.cpanel.domain.users

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "websites")
data class WebSite(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: UUID? = UUID.randomUUID(),

        @Column(name = "server_name")
        var serverName: String? = null,

        @Column(name = "root_path")
        var rootPath: String? = null,

        @Column(name = "engine")
        var engine: String? = null,

        @Column(name = "db_user")
        var dbUser: String? = null,

        @Column(name = "db_name")
        var dbName: String? = null,

        @Column(name = "db_password")
        var dbPassword: String? = null
)
