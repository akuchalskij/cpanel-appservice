package io.upservice.cpanel.dto.websites

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

data class WebSiteDTO(
    @JsonProperty
    var serverName: String? = null,
    @JsonProperty
    var engine: String? = null,
    @JsonProperty
    var dbUser: String? = null,
    @JsonProperty
    var dbName: String? = null,
    @JsonProperty
    var dbPassword: String? = null
) : Serializable