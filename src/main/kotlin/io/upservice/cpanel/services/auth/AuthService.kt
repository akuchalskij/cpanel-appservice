package io.upservice.cpanel.services.auth

import io.upservice.cpanel.dao.users.UserRepository
import io.upservice.cpanel.domain.users.User
import io.upservice.cpanel.dto.auth.UserAuthDto
import io.upservice.cpanel.http.models.HttpMessage
import io.upservice.cpanel.security.JWT
import io.upservice.cpanel.security.JWTProvider

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import java.util.*
import java.util.stream.Collectors

@Service
class AuthService {
    @Autowired
    lateinit var authenticationManager: AuthenticationManager

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var tokenProvider: JWTProvider

    fun authenticate(request: UserAuthDto): ResponseEntity<*> {
        val userCandidate: Optional<User> = userRepository.findByEmail(request.email!!)

        return if (userCandidate.isPresent) {
            val user: User = userCandidate.get()
            val authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(request.email, request.password)
            )

            SecurityContextHolder.getContext().authentication = authentication

            val jwt: String = tokenProvider.createToken(user.email!!)

            val authorities: List<GrantedAuthority> = user.roles!!
                .stream()
                .map { role -> SimpleGrantedAuthority(role.name) }
                .collect(Collectors.toList<GrantedAuthority>())

            ResponseEntity(JWT(jwt, user.email, authorities), HttpStatus.OK)
        } else {
            ResponseEntity(HttpMessage("User not found!"), HttpStatus.BAD_REQUEST)
        }
    }
}