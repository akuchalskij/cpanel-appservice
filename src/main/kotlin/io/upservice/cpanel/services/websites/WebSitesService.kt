package io.upservice.cpanel.services.websites

import io.upservice.cpanel.commands.NginxCommand
import io.upservice.cpanel.dao.users.WebSiteRepository
import io.upservice.cpanel.domain.users.WebSite
import io.upservice.cpanel.dto.websites.WebSiteDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class WebSitesService {
    @Autowired
    lateinit var repository: WebSiteRepository

    lateinit var command: NginxCommand

    fun createWebSite(request: WebSiteDTO) {
        val website = WebSite(
            serverName = request.serverName,
            rootPath = "/var/www/${request.serverName}",
            engine = request.engine,
            dbName = request.serverName,
            dbUser = request.dbUser,
            dbPassword = request.dbPassword
        )

        repository.save(website)

        command.createDomainConfig(website, "noop")

    }
}